//
//  ListViewController.swift
//  heroes
//
//  Created by Pedro Henrique on 25/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import UIKit

class HeroListTableViewController: UITableViewController {
    // MARK: - Properties

    var heroes: [Hero] = []

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()

        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 34, weight: .bold) ]
        navigationItem.largeTitleDisplayMode = .automatic
        
        registerCells()
        getHeroes()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.heroCell, for: indexPath) as! HeroCell
        let hero = heroes[indexPath.row]

        cell.setup(heroName: hero.name, publisher: hero.publisher.name, image: hero.image["icon_url"])

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowHero", sender: self)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? HeroDetailsViewController, let indexPath = tableView.indexPathForSelectedRow {
            destination.hero = heroes[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    // MARK: - Methods

    private func registerCells() {
        let cellNib = UINib(nibName: Constants.CellIdentifiers.heroCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: Constants.CellIdentifiers.heroCell)
    }

    // MARK: - API Methods
    private func getHeroes() {
        URLSession.shared.dataTask(with: URL(string: Constants.URL.charactersUrl)!) { data, response, apiError in
            guard let data = data, apiError == nil, response != nil else {
                print("something is wrong")
                return
            }

            do {
                let json = try JSONDecoder().decode(Heroes.self, from: data)
                self.heroes = json.heroes
            } catch {
                print(error)
            }

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }

        }.resume()
    }

    
}
