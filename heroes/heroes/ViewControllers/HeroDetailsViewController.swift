//
//  crapVC.swift
//  heroes
//
//  Created by Pedro Henrique on 25/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import UIKit

class HeroDetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heroImage: UIImageView!

    var hero: Hero!
    var downloadTask: URLSessionDownloadTask?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = hero.name
        tableView.contentInsetAdjustmentBehavior = .never
        downloadTask = heroImage.loadImage(urlString: hero.image["screen_large_url"]!)

        registerCells()
    }

    deinit {
        downloadTask?.cancel()
        downloadTask = nil
    }

    private func registerCells() {
        let cellNib = UINib(nibName: Constants.CellIdentifiers.detailCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: Constants.CellIdentifiers.detailCell)
    }
}

extension HeroDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Aliases"
        case 1:
            return "Publisher"
        case 2:
            return "Description"
        default:
            return ""
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifiers.detailCell, for: indexPath) as! DetailCell

        let text: String

        switch indexPath.section {
        case 0:
            text = hero.aliases?.replacingOccurrences(of: "\r\n", with: "-") ?? "This character has no aliases"
        case 1:
            text = hero.publisher.name
        case 2:
            text = hero.description ?? "This character doesn`t have a description yet"
        default:
            text = ""
        }

        cell.setDetailText(with: text)
        
        return cell
    }
}
