//
//  Hero.swift
//  heroes
//
//  Created by Pedro Henrique on 26/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import Foundation

class Heroes: Codable {
    let heroes: [Hero]

    private enum CodingKeys: String, CodingKey {
        case heroes = "results"
    }
}

class Hero: Codable {
    let name: String
    var aliases: String?
    var description: String?
    let image: [String : String]
    let publisher: Publisher

    private enum CodingKeys: String, CodingKey {
        case name
        case aliases
        case description = "deck"
        case image
        case publisher
    }
}

class Publisher: Codable {
    let name: String
}
