//
//  Cells.swift
//  heroes
//
//  Created by Pedro Henrique on 25/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import UIKit

class HeroCell: UITableViewCell {
    @IBOutlet weak var heroNameLabel: UILabel!
    @IBOutlet weak var publisherNameLabel: UILabel!
    @IBOutlet weak var heroImage: UIImageView!

    var downloadTask: URLSessionDownloadTask?

    override func prepareForReuse() {
        super .prepareForReuse()
        downloadTask?.cancel()
        downloadTask = nil
        heroImage.image = UIImage(named: "Placeholder")
    }

    func setup(heroName: String, publisher: String, image: String?) {
        heroNameLabel.text = heroName
        publisherNameLabel.text = publisher
        heroImage.layer.cornerRadius = 5

        guard let imageString = image else { return }

        downloadTask = heroImage.loadImage(urlString: imageString)
    }
}

class DetailCell: UITableViewCell {
    @IBOutlet weak var detailLabel: UILabel!

    func setDetailText(with text: String) {
        detailLabel.text = text
    }
}
