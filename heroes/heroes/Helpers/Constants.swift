//
//  Constants.swift
//  heroes
//
//  Created by Pedro Henrique on 25/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import Foundation

struct Constants {
    struct URL {
        static let charactersUrl = "https://comicvine.gamespot.com/api/characters/?api_key=7b34f237adbd96c06afd364515d5f00a2f3f25da&format=json&limit=20&field_list=aliases,deck,name,image,publisher"
    }

    struct CellIdentifiers {
        static let heroCell = "HeroCell"
        static let loadingCell = "LoadingCell"
        static let detailCell = "DetailCell"
    }
}
