//
//  UIImageView+DownloadImage.swift
//  heroes
//
//  Created by Pedro Henrique on 26/05/19.
//  Copyright © 2019 Pedro Henrique. All rights reserved.
//

import UIKit

//extension UIImageView {
//    func loadImage(urlString: String) -> URLSessionDownloadTask? {
//        let imageCache = NSCache<NSString, UIImage>()
//        let session = URLSession.shared
//        let imageURL = URL(string: urlString)!
//
//        if let cachedImage = imageCache.object(forKey: NSString(string: urlString)) {
//            self.image = cachedImage
//            return nil
//        }
//
//        let downloadTask = session.downloadTask(with: imageURL, completionHandler: { [weak self] url, response, error in
//            if error == nil, let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    if let weakSelf = self {
//                        imageCache.setObject(image, forKey: NSString(string: urlString))
//                        weakSelf.image = image
//                    }
//                }
//            }
//        })
//
//        downloadTask.resume()
//
//        return downloadTask
//    }
//
//    func transition(toImage image: UIImage?) {
//        UIView.transition(with: self, duration: 0.3, options: [.transitionCrossDissolve], animations: { self.image = image }, completion: nil)
//    }
//}

//extension UIImageView {
//    func loadImage(fromURL url: String) {
//        guard let imageURL = URL(string: url) else {
//            return
//        }
//
//        let cache =  URLCache.shared
//        let request = URLRequest(url: imageURL)
//        DispatchQueue.main.async {
//            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    self.transition(toImage: image)
//                }
//            } else {
//                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
//                        let cachedData = CachedURLResponse(response: response, data: data)
//                        cache.storeCachedResponse(cachedData, for: request)
//                        DispatchQueue.main.async {
//                            self.transition(toImage: image)
//                        }
//                    }
//                }).resume()
//            }
//        }
//    }
//
//    func transition(toImage image: UIImage?) {
//        UIView.transition(with: self, duration: 0.3, options: [.transitionCrossDissolve], animations: { self.image = image }, completion: nil)
//    }
//}

extension UIImageView {
    func loadImage(urlString: String) -> URLSessionDownloadTask {
        let imageURL = URL(string: urlString)!
        let session = URLSession.shared

        let downloadTask = session.downloadTask(with: imageURL, completionHandler: { [weak self] url, response, error in
            if error == nil, let url = url,
                let data = try? Data(contentsOf: url),
                let image = UIImage(data: data) {

                DispatchQueue.main.async {
                    if let weakSelf = self {
                        weakSelf.image = image
                        self?.transition(toImage: image)
                    }
                } }
        })

        downloadTask.resume()

        return downloadTask
    }

    public func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.2, options: [.transitionCrossDissolve], animations: {
            self.image = image
        }, completion: nil)
    }
}
